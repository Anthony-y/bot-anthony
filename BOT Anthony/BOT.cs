﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Linq;

using SteamKit2;
using System.Security.Cryptography;
using SteamKit2.Internal;

using Newtonsoft.Json;

namespace BOT_Anthony
{
    public class Bot
    {
        static string usrName;
        static string usrPass;
        static string authCode;
        static string messageName;

        static string defaultConfig;

        static bool isRunning = false;
        static bool shouldAwayMessage = true;
        static bool isRPS = false;

        static string[] swearWords;

        static SteamClient steamClient;
        static SteamUser steamUser;
        static CallbackManager callBackManager;
        static SteamFriends steamFriends;

        static SteamID AdminID;

        static Config config = new Config();

        private static void Main(string[] args)
        {
            Console.Title = "BOT Anthony";
            Console.WriteLine("Ctrl + C quits m8");

            defaultConfig = "{ \"Username\": \"username\", \"Password\": \"password\", \"Bot Name\": \"BOT Default\", \"Admin ID\": \"adminID\" }";

            if (!File.Exists("config.json"))
            {
                File.Create("config.json");

                using (var stream = File.Open("config.json", FileMode.Open, FileAccess.Write))
                {
                    stream.Write(Encoding.UTF8.GetBytes(defaultConfig), 0, Encoding.UTF8.GetByteCount(defaultConfig));
                }

                Console.WriteLine("[BOT] Created default configuration file \"config.json\".");
                Console.WriteLine("[BOT] Values are still default. Please exit and modify.");
                Thread.Sleep(TimeSpan.FromSeconds(1));
                Environment.Exit(1);
            } else
            {
                config = JsonConvert.DeserializeObject<Config>(File.ReadAllText("config.json"));
                AdminID = new SteamID(config.AdminID);
                if (config.Swears.Length > 0) swearWords = config.Swears;
                Console.WriteLine("[BOT] Loaded JSON configuration file sucessfully!");
            }

            if (config != null)
            {
                Console.WriteLine("[Authentication] Grabbing username and password from file...");
                usrName = config.Username;
                usrPass = config.Password;
                Console.WriteLine("[Authentication] Retrieved username and password.");
            } else
            {
                Console.WriteLine("[Authentication] Please enter a username and password into \"config.json\".");
                Thread.Sleep(TimeSpan.FromSeconds(1));
                Environment.Exit(1);
            }

            Login();
        }

        private static void Login()
        {
            steamClient = new SteamClient();
            callBackManager = new CallbackManager(steamClient);
            steamFriends = steamClient.GetHandler<SteamFriends>();
            steamUser = steamClient.GetHandler<SteamUser>();

            callBackManager.Subscribe<SteamClient.ConnectedCallback>(OnConnected);
            callBackManager.Subscribe<SteamUser.LoggedOnCallback>(OnLoggedIn);
            callBackManager.Subscribe<SteamUser.UpdateMachineAuthCallback>(OnMachineAuth);
            callBackManager.Subscribe<SteamClient.DisconnectedCallback>(OnDisconnected);
            callBackManager.Subscribe<SteamFriends.FriendMsgCallback>(OnMsgRecieved);
            callBackManager.Subscribe<SteamUser.AccountInfoCallback>(OnAccountInfo);
            callBackManager.Subscribe<SteamFriends.FriendsListCallback>(OnFriendsList);
            callBackManager.Subscribe<SteamFriends.FriendAddedCallback>(OnFriendAdded);
            callBackManager.Subscribe<SteamFriends.PersonaStateCallback>(OnFriendPersonaChange);

            SteamDirectory.Initialize().Wait();
            steamClient.Connect();

            isRunning = true;

            while (isRunning)
            {
                callBackManager.RunWaitCallbacks(TimeSpan.FromSeconds(0.5));
            }

            Console.ReadKey();
        }

        private static void OnConnected(SteamClient.ConnectedCallback callback)
        {
            if (callback.Result != EResult.OK)
            {
                Console.WriteLine("[Steam] Could not connect to Steam.");
                Console.WriteLine($"({callback.Result})");
                isRunning = false;
                return;
            }

            Console.WriteLine("[Steam] Logging in.");

            byte[] sentryHash = null;
            if (File.Exists("sentry.bin"))
            {
                // if we have a saved sentry file, read and sha-1 hash it
                byte[] sentryFile = File.ReadAllBytes("sentry.bin");
                sentryHash = CryptoHelper.SHAHash(sentryFile);
            }

            steamUser.LogOn(new SteamUser.LogOnDetails()
            {
                Username = usrName,
                Password = usrPass,
                AuthCode = authCode,
                SentryFileHash = sentryHash,
            });
        }

        private static void OnMachineAuth(SteamUser.UpdateMachineAuthCallback callback)
        {
            Console.WriteLine("[Authentication] Updating 'sentry' file..");

            int fileSize;
            byte[] sentryHash;
            using (var fs = File.Open("sentry.bin", FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                fs.Seek(callback.Offset, SeekOrigin.Begin);
                fs.Write(callback.Data, 0, callback.BytesToWrite);
                fileSize = (int)fs.Length;

                fs.Seek(0, SeekOrigin.Begin);
                using (var sha = new SHA1CryptoServiceProvider())
                {
                    sentryHash = sha.ComputeHash(fs);
                }
            }

            // inform the steam servers that we're accepting this sentry file
            steamUser.SendMachineAuthResponse(new SteamUser.MachineAuthDetails
            {
                JobID = callback.JobID,

                FileName = callback.FileName,

                BytesWritten = callback.BytesToWrite,
                FileSize = fileSize,
                Offset = callback.Offset,

                Result = EResult.OK,
                LastError = 0,

                OneTimePassword = callback.OneTimePassword,

                SentryFileHash = sentryHash,
            });

            Console.WriteLine("[Authentication] Done.");
        }

        private static void OnLoggedIn(SteamUser.LoggedOnCallback callback)
        {
            bool isSteamGuard = callback.Result == EResult.AccountLogonDenied;

            if (isSteamGuard)
            {
                Console.WriteLine("[Authentication] This account is SteamGuard protected!");
                Console.Write("[Authentication] Please enter the auth code sent to the email (provider: {0}): ", callback.EmailDomain);
                authCode = Console.ReadLine();

                return;
            }

            if (callback.Result != EResult.OK)
            {
                Console.WriteLine($"[Authentication] Unable to logon to Steam: {callback.Result}");

                isRunning = false;
                return;
            }

            Console.WriteLine("[Steam] Connected successfully to Steam network!\n");

            steamFriends.SetPersonaName(config.BotName);
        }

        private static void OnDisconnected(SteamClient.DisconnectedCallback callback)
        {
            Console.WriteLine("[Steam] Disconnected from Steam. Reconnecting in 5 secs.");
            Thread.Sleep(TimeSpan.FromSeconds(5));
            steamClient.Connect();
            Console.WriteLine("[Steam] Retrying connection...");
        }

        private static void OnMsgRecieved(SteamFriends.FriendMsgCallback callback)
        {
            if (callback.EntryType == EChatEntryType.ChatMsg)
            {
                var finalMsg = callback.Message.Trim();
                var splitMessage = finalMsg.Split(' ');
                messageName = steamFriends.GetFriendPersonaName(callback.Sender);

                Console.WriteLine($"[Steam Friends] Message recieved from {callback.Sender} ({messageName}): {callback.Message}");

                switch (splitMessage[0].ToLower())
                {
                    case "!help":
                        if (isRPS) isRPS = false;

                        steamFriends.SendChatMessage
                        (
                            callback.Sender,
                            EChatEntryType.ChatMsg,
                            $"Hi, I'm {config.BotName}. I'm a chat bot made by a fine fellow called Anthony. \nHere are the things I can do: \n1. !help - I guess you already found this one huh. \n2. !time - I can tell you your machine time.\n3. !date - I can tell you the current date.\n4. !steamid - View your unique Steam hash.\n5. !accountid - View your account's unique Account ID.\n6. !friends - View my friends list.\n7. !rps - play rock, paper, scissors with me."
                        );
                        Console.WriteLine($"[Steam Friends] Issued help to user {messageName}.");
                        break;

                    case "!time":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "The time is: " + DateTime.Now.ToShortTimeString());
                        Console.WriteLine($"[Steam Friends] Issued time to user {messageName}.");
                        break;

                    case "!date":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "The date is: " + DateTime.Today.ToShortDateString());
                        Console.WriteLine($"[Steam Friends] Issued date to user {messageName}.");
                        break;

                    case "!changename":
                        if (isRPS) isRPS = false;
                        if (callback.Sender != AdminID)
                        {
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Nice try m80, only Anthony can change my name.");
                        }
                        else
                        {
                            var nameBuilder = new StringBuilder();

                            for (int i = 1; i < splitMessage.Length; i++) { nameBuilder.Append(splitMessage[i] + " "); }
                            steamFriends.SetPersonaName(nameBuilder.ToString());

                            config.Username = nameBuilder.ToString();

                            using (var stream = File.OpenWrite("config.json"))
                            {
                                stream.Write(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(config)), 0, Encoding.UTF8.GetByteCount(defaultConfig));
                            }

                            Console.WriteLine($"[Steam Friends] User {messageName} changed name to {steamFriends.GetPersonaName()}.");
                        }
                        break;

                    case "!steamid":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Your Steam Hash is: " + callback.Sender);
                        Console.WriteLine($"[Steam Friends] Issued {messageName} their Steam ID.");
                        break;

                    case "!accountid":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Your Account ID is: " + callback.Sender);
                        Console.WriteLine($"[Steam Friends] Issued {messageName} their Steam ID.");
                        break;

                    case "!friends":
                        if (isRPS) isRPS = false;
                        var botFriends = new List<SteamID>();
                        var friendListBuilder = new StringBuilder();

                        for (int i = 0; i < steamFriends.GetFriendCount(); i++)
                        {
                            var listFriend = steamFriends.GetFriendByIndex(i);
                            if (i == steamFriends.GetFriendCount() - 1) // if were on the second to last name in our friends list...
                            {
                                // add an "and" a "."
                                friendListBuilder.Append("and " + steamFriends.GetFriendPersonaName(listFriend) + ".");
                            }
                            else { friendListBuilder.Append(steamFriends.GetFriendPersonaName(listFriend) + ", "); }
                        }

                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "I'm friends with: " + friendListBuilder.ToString());

                        Console.WriteLine($"[Steam Friends] Issued {messageName} bot friends list.");
                        break;

                    case "!send":
                        if (isRPS) isRPS = false;
                        var messageBuilder = new StringBuilder();
                        var sendFriend = new SteamID(splitMessage[1]);

                        for (int i = 2; i < splitMessage.Length; i++) { messageBuilder.Append(splitMessage[i] + " "); }

                        steamFriends.SendChatMessage(sendFriend, EChatEntryType.ChatMsg, messageBuilder.ToString() + $" - from {messageName}.");

                        Console.WriteLine($"[Steam Friends] User {messageName} sent message \"{messageBuilder}\" to user {steamFriends.GetFriendPersonaName(sendFriend)}.");
                        break;

                    case "!awaymsg":
                        if (isRPS) isRPS = false;
                        if (callback.Sender != AdminID) { steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, $"Nice try, only {steamFriends.GetFriendPersonaName(AdminID)} can change the away message."); }
                        else
                        {
                            shouldAwayMessage = !shouldAwayMessage;

                            if (shouldAwayMessage) { steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Away message enabled."); }
                            else if (!shouldAwayMessage) { steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Away message disabled."); }

                            Console.WriteLine($"[Steam Friends] {messageName} changed away message to {shouldAwayMessage.ToString().ToLower()}.");
                        }

                        break;

                    case "!getpic":
                        if (isRPS) isRPS = false;
                        var userID = new SteamID(splitMessage[1]);
                        var avatarHash = BitConverter.ToString(steamFriends.GetFriendAvatar(userID)).Replace("-", "").ToLower();
                        var webClient = new WebClient();
                        webClient.DownloadFile("https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/a0/" + avatarHash + "_full.jpg", messageName + "_avatar.jpg");
                        webClient.Dispose();

                        break;

                    case "!badrats":
                        if (isRPS) isRPS = false;
                        var playGame = new ClientMsgProtobuf<CMsgClientGamesPlayed>(EMsg.ClientGamesPlayed);
                        var playingBadRats = playGame.Body.games_played.FirstOrDefault(gm => gm.game_id == 34900);

                        // Essentially, if we are playing Bad Rats.
                        if (playingBadRats != null)
                        {
                            // Stop playing Bad Rats :P
                            playGame.Body.games_played.Remove(playingBadRats);
                            Console.WriteLine("[Steam] No longer playing Bad Rats!");
                        }
                        else
                        {
                            // If we're not playing Bad Rats
                            playGame.Body.games_played.Add(new CMsgClientGamesPlayed.GamePlayed
                            {
                                game_id = new GameID(34900),
                            });
                            steamClient.Send(playGame);

                            Console.WriteLine("[Steam] Now playing Bad Rats!");
                        }

                        break;

                    case "!rps":
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Alright, let's play. What do you pick?");

                        isRPS = true;

                        break;

                    case "rock":

                        if (isRPS)
                        {
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "I pick paper.");
                            Thread.Sleep(500);
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "https://youtu.be/x35P2wTX0zg?t=1m17s");
                            isRPS = false;
                        }

                        break;

                    case "paper":

                        if (isRPS)
                        {
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "I pick scissors.");
                            Thread.Sleep(500);
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "https://youtu.be/x35P2wTX0zg?t=1m17s");
                            isRPS = false;
                        }

                        break;

                    case "scissors":

                        if (isRPS)
                        {
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "I pick rock.");
                            Thread.Sleep(500);
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "https://youtu.be/x35P2wTX0zg?t=1m17s");
                            isRPS = false;
                        }

                        break;

                    case "spok":
                        if (isRPS)
                        {
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Nah you can't do that. I pick Lizard. How'd ya like that?");
                            Thread.Sleep(500);
                            steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "https://youtu.be/x35P2wTX0zg?t=1m17s");
                        }

                        break;

                    case "hi":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, $"Hi, {messageName}");
                        break;

                    case "hey":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, $"Hi, {messageName}");
                        break;

                    case "yo":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, $"Hi, {messageName}");
                        break;

                    case "hello":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, $"Hi, {messageName}");
                        break;

                    case "die":
                        if (isRPS) isRPS = false;
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Fuck you. Worthless piece of shit. You're talking to a fucking robot. Die in a fire fagut.");
                        break;
                }

                foreach (var s in swearWords)
                {
                    if (finalMsg.ToLower().Contains(s))
                    {
                        steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Don't talk to me like that!");
                    }
                }
            }
        }

        private static void OnFriendsList(SteamFriends.FriendsListCallback callback)
        {
            foreach (var f in callback.FriendList)
            {
                var name = steamFriends.GetFriendPersonaName(f.SteamID);

                if (f.Relationship == EFriendRelationship.RequestRecipient)
                {
                    steamFriends.AddFriend(f.SteamID);
                    Console.WriteLine($"[Steam Friends] Added friend {messageName}");
                }
            }
        }

        private static void OnFriendAdded(SteamFriends.FriendAddedCallback callback)
        {
            steamFriends.SendChatMessage
            (
                callback.SteamID,
                EChatEntryType.ChatMsg,
                $"Hi, {callback.PersonaName}, you're now friends with BOT Patrick. I'm actually a bot and this message is automated. Just thought you should know that. (Type !help to see what I can do ( ͡° ͜ʖ ͡°))"
            );
        }

        private static void OnAccountInfo(SteamUser.AccountInfoCallback callback)
        {
            steamFriends.SetPersonaState(EPersonaState.Online);
            if (steamFriends.GetPersonaName() == "BOT Ken") { steamFriends.SetPersonaName("BOT Ken"); }
        }

        private static void OnFriendPersonaChange(SteamFriends.PersonaStateCallback callback)
        {
            if (shouldAwayMessage)
            {
                var name = steamFriends.GetFriendPersonaName(callback.FriendID);

                if (callback.State == EPersonaState.Away)
                {
                    steamFriends.SendChatMessage(callback.FriendID, EChatEntryType.ChatMsg, $"See you soon, {messageName}!");
                }

                if (callback.State == EPersonaState.Busy)
                {
                    steamFriends.SendChatMessage(callback.FriendID, EChatEntryType.ChatMsg, $"Busy eh? ( ͡° ͜ʖ ͡°)");
                }

                if (callback.State == EPersonaState.Snooze)
                {
                    steamFriends.SendChatMessage(callback.FriendID, EChatEntryType.ChatMsg, $"Oi. Wake up.");
                }
            }
        }
    }
}
