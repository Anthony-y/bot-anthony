﻿using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BOT_Anthony
{
    internal class Config
    {
        [JsonProperty("Username")]
        public string Username { get; set; } = "username";

        [JsonProperty("Password")]
        public string Password { get; set; } = "password";

        [JsonProperty("Bot Name")]
        public string BotName { get; set; } = "BOT";

        [JsonProperty("Admin ID")]
        public string AdminID { get; set; } = "adminID";

        [JsonProperty("Swears")]
        public string[] Swears { get; set; } = new string[] { };

        [JsonProperty("Greeting Name")]
        public string GreetingName { get; set; } = "Owner";

    }
}
